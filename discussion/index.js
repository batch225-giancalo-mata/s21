// console.log("Hello")

//[SECTION] ARRAY
//An array in programming is simply a list of data.

// They are declared using square bracket [] also known as "Array Literals"

// Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks.

/*
	- Syntax:
	let/const arrayName = [elementA, elementB, elementC...];

*/

let	studentNumberA = '2020-1923';
let	studentNumberB = '2020-1924';
let	studentNumberC = '2020-1925';
let	studentNumberD = '2020-1926';
let	studentNumberE = '2020-1927';

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

console.log(studentNumbers);

// Common example of arrays

let	grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// Possible use of an array but some of this is not recommended.

// empty string ""; empty array [], empty object {}


let	mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative way to write arrays

let myTasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake sass'
];

//Creating an array with values from variables:
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1,city2,city3];

console.log(myTasks);
console.log(cities);

//[SECTION] .length property

// The .length property allows us to get and set the total numbers of items in an array.

//Delete last array using .length

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

myTasks.length = myTasks.length-1;
console.log(myTasks.length)
console.log(myTasks);

cities.length--;
console.log(cities);

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++
console.log(theBeatles);

//[SECTION] Reading from Arrays
// Accesssing array elements is one of the more common tasks that we do with an array. This can be done through the use of array indexes.

/*
	SYNTAX:
		arrayName[index];
*/

console.log(grades[0]);
console.log(computerBrands[3]);
console.log(grades[20]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"]

console.log('Array before reassignment');
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log('Array after reassignment')
console.log(lakersLegends);

//Adding an Array

let newArr = [];
console.log(newArr[0]);

//newArr[0] is undefined because we haven't yet defined the item/data for that index, we can update the index with a new value:

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

newArr.push("Food");
console.log(newArr);

//for loops in array

for (let index = 0; index < newArr.length; index++){

	console.log(newArr[index]);
}

// For loops and if else in an array

// Given an array of numbers, you can also show if the following iems in the array are divisible by 5 or not. You can mix in an if-else statement in the loop:

let numArr = [5,12,30,46,40];

for(let index = 0; index < numArr.length; index++){

    if(numArr[index] % 5 === 0){

        console.log(numArr[index] + " is divisible by 5");

    } else {

        console.log(numArr[index] + " is not divisible by 5");

    }
}


//[SECTION] Multidimentional Arrays

/*
	- Multidimensional arrays are useful for storing complex data structures.
	- A practical application of this is to help visualize or create real world objects.
	- Though useful in a number of cases, creating complex array structures is not always recommended.

*/

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);

// Accessing elements if a multidimentional arrays

console.log(chessBoard[1][4]);

console.log("Pawn moves to: " + chessBoard[1][5]);